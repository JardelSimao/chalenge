"use strict";

angular.module("services", [])

.factory('chalengeService', function(Restangular) {
	
	var str = "863d557bc2d3eaa432821:9ad39a8d9643e964328";
	var enc = window.btoa(str);
	Restangular.setDefaultHeaders({Authorization: 'Basic '+enc});

	var _getContacts = function(){
		return Restangular.all('contact').getList();
	};

	var _getContactOne = function(id){
		return Restangular.one('contact', id).get();
	};

	var _editContact = function(contact){
		var id = contact.id;
		var obj = {
			name: contact.name,
			email: contact.email,
			phone: contact.phone,
			twitter: contact.twitter
		}
		return Restangular.one('contact', id).patch(obj);
	};

	var _delContact = function(id){
		return Restangular.one('contact', id).remove();
	};

	var _addContacts = function(data){
		return Restangular.all('contact').post(data);
	};

	return {

		getContacts: _getContacts,
		getContactOne: _getContactOne,
		editContact: _editContact,
		delContact: _delContact,
		addContacts: _addContacts

	}

})