"use strict";
angular.module("controllers", [])

.controller("HomeController", function($scope,chalengeService,$state,$stateParams){

	chalengeService.getContacts().then(function(response){
		$scope.contacts = response;
	});

  $scope.Add = function(){
    $state.go('add');
  }

  $scope.Details = function(id){
    $state.go('details', {id:id});
  }

})

.controller("DetailsController", function($scope,chalengeService,$state,$stateParams,$mdDialog){

  $scope.showConfirm = function(ev, id) {

    var confirm = $mdDialog.confirm()
          .title('Confirmação de exclusão')
          .textContent('Você tem certeza que deseja excluir este contato?')
          .targetEvent(ev)
          .ok('Sim')
          .cancel('Cancelar');

    $mdDialog.show(confirm).then(function() {
      $scope.Del(id);
    }, function() {

    });
  };

  var id = $stateParams.id;

  $scope.List = function(){
    $state.go('home');
  }

  $scope.Edit = function(id){
    $state.go('edit', {id:id});
  }

  chalengeService.getContactOne(id).then(function(response){
    $scope.contact = response;
  });

  $scope.Del = function(id){
    chalengeService.delContact(id).then(function(){
        $state.go('home');
    });
  }

})

.controller("AddContactController", function($scope,chalengeService,$state){

  $scope.List = function(){
    $state.go('home');
  }

  $scope.addContact = function(contact){
    chalengeService.addContacts(contact);
    $state.go('home');
  }

})

.controller("EditContactController", function($scope,chalengeService,$state,$stateParams){

  var id = $stateParams.id;

  chalengeService.getContactOne(id).then(function(response){
    $scope.contact = response;
  });
  
  $scope.List = function(){
    $state.go('home');
  }

  $scope.editContact = function(contact){
    chalengeService.editContact(contact).then(function(){
        $state.go('home');
    });
  }

});