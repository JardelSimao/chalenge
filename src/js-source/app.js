"use strict";
angular.module("chalenge", ["ui.router","ngMessages","ngMaterial","controllers","services",'restangular'])

.config(function($mdThemingProvider,$stateProvider,$sceDelegateProvider,$urlRouterProvider,RestangularProvider){
$mdThemingProvider.theme('default')
    .primaryPalette('blue-grey')
    .accentPalette('orange');

	RestangularProvider.setBaseUrl('https://pipz.io/api/v1');

	RestangularProvider.addResponseInterceptor(function(data, operation){                                                
        if (operation === "getList"){
            var newResponse = data.objects;
            return newResponse;
        }
        return data;
    });

	$urlRouterProvider.otherwise('/home');
	$stateProvider
		.state("home", {
			url: "/home",
			controller: "HomeController",
			templateUrl: "js/views/home.html",
		})
		.state("details", {
			url: "/details",
			controller: "DetailsController",
			templateUrl: "js/views/details.html",
			params: {
		        id: null
		    }
		})
		.state("add", {
			url: "/add",
			controller: "AddContactController",
			templateUrl: "js/views/add.html",
		})
		.state("edit", {
			url: "/edit",
			controller: "EditContactController",
			templateUrl: "js/views/edit.html",
			params: {
		        id: null
		    }
		});

})